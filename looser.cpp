#include <bits/stdc++.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string>
#include <curses.h>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <unistd.h>

using namespace std;

void red()
{
    printf("\033[1;31m");
}

void lightred()
{
    printf("\033[0;31m");
}

void green()
{
    printf("\033[1;32m");
}

void lightgreen()
{
    printf("\033[0;32m");
}

void yellow()
{
    printf("\033[0;33m");
}

void cyan()
{
    printf("\033[1;36m");
}

void lightcyan()
{
    printf("\033[0;36m");
}

void magneta()
{
    printf("\033[1;35m");
}

void lightmagneta()
{
    printf("\033[0;35m");
}

void blue()
{
    printf("\033[1;34m");
}

void lightblue()
{
    printf("\033[0;34m");
}

void looser()
{
    cout << "         __        ______     ______        _______. _______ .______ " << endl;

    cout << "       |  |      /  __  \\   /  __  \\      /       ||   ____||   _  \\ " << endl;

    cout << "       |  |     |  |  |  | |  |  |  |    |   (----`|  |__   |  |_)  |  " << endl;

    cout << "       |  |     |  |  |  | |  |  |  |     \\   \\    |   __|  |      /   " << endl;

    cout << "       |  `----.|  `--'  | |  `--'  | .----)   |   |  |____ |  |\\  \\----." << endl;

    cout << "       |_______| \\______/   \\______/  |_______/    |_______|| _| `._____|" << endl;
}

void newYear()
{

    cout << " _    _                   __     __      _   _                          __     __    " << endl;
    cout << "| |  | |                  \\ \\   / /    | \\ | |                       \\ \\   / /    " << endl;
    cout << "| |__| |  __ _  _ __   _ __\\ \\_/ /     |  \\| |  ___ __      __        \\ \\_/ /___   __ _  _ __ " << endl;
    cout << "|  __  | / _` || '_ \\ | '_ \\   /       | . ` | / _ \\ \\ /\\ / /          \\    / _ \\ / _` || '__|" << endl;
    cout << "| |  | || (_| || |_) || |_) || |       |\\  ||  __/ \\ V  V /     |        ||  __/| (_| || |   " << endl;
    cout << "|_|  |_| \\__,_|| .__/ | .__/ |_|       |_| \\_| \\___|  \\_/\\_/            |_| \\___| \\__,_||_|   " << endl;
    cout << "               | |    | |                                                            " << endl;
    cout << "               |_|    |_|                                                            " << endl;
}

int main()
{
    time_t t = time(NULL);
    tm *timePtr = localtime(&t);

    lightmagneta();
    cout << "day of month = " << (timePtr->tm_mday) << endl;
    cout << "month of year = " << (timePtr->tm_mon) + 1 << endl;
    cout << "year = " << (timePtr->tm_year) + 1900 << endl;
    cout << "weekday = " << (timePtr->tm_wday);

    if (timePtr->tm_wday == 1)
    {
        cout << " (Monday) " << endl;
    }
    else if (timePtr->tm_wday == 2)
    {
        cout << " (Tuesday) " << endl;
    }
    else if (timePtr->tm_wday == 3)
    {
        cout << "  (Wednesday) " << endl;
    }
    else if (timePtr->tm_wday == 4)
    {
        cout << " (Thursday) " << endl;
    }
    else if (timePtr->tm_wday == 5)
    {
        cout << " (Friday) " << endl;
    }
    else if (timePtr->tm_wday == 6)
    {
        cout << " (Saturday) " << endl;
    }
    else if (timePtr->tm_wday == 0)
    {
        cout << " (Sunday) " << endl;
    }
    else
    {
        cout << "  Day Not Found  " << endl;
    }

    cout << "day of year = " << (timePtr->tm_yday) << endl;
    cout << "daylight savings = " << (timePtr->tm_isdst) << endl;
    cout << endl;
    cout << endl;

    cout << "Date     " << (timePtr->tm_mday) << "/" << (timePtr->tm_mon) + 1 << "/" << (timePtr->tm_year) + 1900 << endl;
    cout << "Time     " << (timePtr->tm_hour) << ":" << (timePtr->tm_min) << ":" << (timePtr->tm_sec) << endl;
     
   


    if (timePtr->tm_hour >= 04 && timePtr->tm_hour < 12)
    {
        cout << "Good Afternoon Looser" << endl;
    }
    else if (timePtr->tm_hour >= 12 && timePtr->tm_hour < 17)
    {
        cout << "Good Afternoon Looser" << endl;
    }
    else
    {
        cout << "Good Evening Looser" << endl;
    }

    green();
    looser();
    sleep(2);
    cout<<""<<endl;
    cout<<""<<endl;
    cout<<""<<endl;

    if (timePtr->tm_yday == 0)
    {
        red();
        newYear();
    }
    cout<<""<<endl;

    red();
    cout << "Press Enter To Continue....." << endl;
    cin.get();

    lightblue();
    cout<<"                              Welcome To Personal Tools"<<endl;



    cout<<"             You Can Do File , Directory and Emails Using Following Commands."<<endl;
    sleep(3);
    cout<<""<<endl;
    
    lightcyan();
    cout<<"\nFile Operations...";
    yellow();
    cout<<"\n 1.To Create A File Just Type crtf. "<<endl;
    cout<<"\n 2.To Delete A File Just type dltf."<<endl;
    cout<<"\n 3.To Read A File Just Type rdf."<<endl;
    sleep(3);

    cout<<""<<endl;

    lightcyan();
    cout<<"\nDirectory Operations...";
    yellow();
    cout<<"\n 1.To Make A Directory Just Type mkd."<<endl;
    cout<<"\n 2.To Delete A Directory Just Type dld."<<endl;
    cout<<"\n 3.To Read A Directory Just Type rdd."<<endl;
    cout<<"\n 4.To View The Current Directory Type curdir."<<endl;
    sleep(3);

    cout<<""<<endl;


    lightcyan();
    cout<<"\nMail Operations...";
    yellow();
    cout<<"\n 2.To Send Or Recive Mail Type mail"<<endl;
    


}
